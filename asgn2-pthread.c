#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <math.h>
#include "util.h"

int *cutArray;
// Shared resources
int numCount = 0;
int *numArray;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// For threads indication
int *threadAvail;
int threadCount;
Point * copied;

typedef struct node{
    int data;
} node;

typedef struct threadInput{
	int threadNum;
	int totalDimension;
	int currentLocation;
} inthread;

int findAvailablePthread(){
	for(int i=0; i<threadCount; i++){
		if(threadAvail[i] != 0)
			return i;
	}
	return -1;
}

int cmpfunc (const void * a, const void * b){
   return ( *(int*)a - *(int*)b );
}

void * threadCompare(void * input){
	struct threadInput *inputSet = input;
	int dim = inputSet->totalDimension;
	int i = inputSet->currentLocation;
	bool indicator[2];
	for(int j=i+1; j<numCount; j++){
		indicator[0] = true; indicator[1] = true;
    if(numArray[j] == -1)
      continue;
		for(int k=0; k<dim; k++){
			if(indicator[0] & numArray[i] != -1 & numArray[j] != -1)
				indicator[0] = indicator[0] & copied[numArray[i]].values[k] > copied[numArray[j]].values[k];
			else
				indicator[0] = false;
			if(indicator[1] & numArray[i] != -1 & numArray[j] != -1)
				indicator[1] = indicator[1] & copied[numArray[i]].values[k] < copied[numArray[j]].values[k];
			else
				indicator[1] = false;
			if(!indicator[0] & !indicator[1])
				break;
		}
		if(indicator[0]){
			pthread_mutex_lock(&mutex);
			numArray[i] = -1;
			pthread_mutex_unlock(&mutex);
			break;
		}
		if(indicator[1]){
			pthread_mutex_lock(&mutex);
			numArray[j] = -1;
			pthread_mutex_unlock(&mutex);
			continue;
		}
	}

	threadAvail[inputSet->threadNum] = -1;
	pthread_exit(NULL);
}

int asgn2_pthread(Point * points, Point ** pPermissiblePoints, int number, int dim, int thread_number){

	// Initialization
	pthread_mutex_init(&mutex, NULL);
	copied = points;
	pthread_t *pid = malloc(sizeof(pthread_t) * thread_number);
	threadCount = thread_number;
	threadAvail = malloc(sizeof(int) * thread_number);
	numArray = malloc(sizeof(int) * number);
	int cutCount = 0;
	bool indicator[2] = {true, true};

	// Enlarge floating values by 100000
	for(int i=0; i<number; i++)
		for(int j=0; j<dim; j++)
			points[i].values[j] *= 100000;

	for(int i=0; i<thread_number; i++)
		threadAvail[i] = 1;

	int permissiblePointNum = 0;
	Point * permissiblePoints = NULL;

	for(int i=0; i<number; i++)
		numArray[i] = i;
	numCount = number;
	//


	for(int i=0; i<numCount; i++){
		int availThreadNum = findAvailablePthread();
		if(numArray[i] == -1)
			continue;
		if(availThreadNum == -1){
			// No free thread
			for(int j=i+1; j<numCount; j++){
				indicator[0] = true; indicator[1] = true;
				if(numArray[j] == -1)
					continue;
				for(int k=0; k<dim; k++){
					if(indicator[0] & numArray[i] != -1 & numArray[j] != -1)
						indicator[0] = indicator[0] & points[numArray[i]].values[k] > points[numArray[j]].values[k];
					else
						indicator[0] = false;
					if(indicator[1] & numArray[i] != -1 & numArray[j] != -1)
						indicator[1] = indicator[1] & points[numArray[i]].values[k] < points[numArray[j]].values[k];
					else
						indicator[1] = false;
					if(!indicator[0] & !indicator[1])
						break;
				}
				if(indicator[0]){
					pthread_mutex_lock(&mutex);
					numArray[i] = -1;
					pthread_mutex_unlock(&mutex);
					break;
				}
				if(indicator[1]){
					pthread_mutex_lock(&mutex);
					numArray[j] = -1;
					pthread_mutex_unlock(&mutex);
					continue;
				}
			}
		}
		else if(threadAvail[availThreadNum] == 1){
			// Free thread without used
			struct threadInput *input = malloc(sizeof(struct threadInput));
			input->threadNum = availThreadNum;
			input->totalDimension = dim;
			input->currentLocation = i;
			threadAvail[availThreadNum] = 0;
			pthread_create(&pid[availThreadNum], NULL, threadCompare, input);
		}
		else if(threadAvail[availThreadNum] == -1){
			// Free thread but require pthread_join
			pthread_join(pid[availThreadNum], NULL);
			struct threadInput *input = malloc(sizeof(struct threadInput));
			input->threadNum = availThreadNum;
			input->totalDimension = dim;
			input->currentLocation = i;
			threadAvail[availThreadNum] = 0;
			pthread_create(&pid[availThreadNum], NULL, threadCompare, input);

		}
	}

	for(int i=0; i<thread_number; i++)
		if(threadAvail[i] < 1)
			pthread_join(pid[i], NULL);

	cutArray = malloc(sizeof(int) * numCount);

	for(int i=0; i<numCount; i++){
		if(numArray[i] != -1){
			cutArray[cutCount] = numArray[i];
			cutCount += 1;
		}
	}

	permissiblePointNum = cutCount;
	permissiblePoints = malloc(sizeof(Point) * cutCount);
	for(int i=0; i<cutCount; i++)
		permissiblePoints[i] = points[cutArray[i]];

	free(pid);
	free(threadAvail);
	free(numArray);
	free(cutArray);

	*pPermissiblePoints = permissiblePoints;
	return permissiblePointNum;
}
